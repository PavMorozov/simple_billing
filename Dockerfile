FROM python:3.9.6

EXPOSE 8000
RUN mkdir /code
WORKDIR /code

RUN pip install pipenv
COPY Pipfile* /code/
RUN cd /code && pipenv install --system --deploy --ignore-pipfile
COPY . /code/
