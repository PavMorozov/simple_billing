## Getting started
Install Docker and Docker Compose, then:

Copy example.env to env file and fill your data

```bash
cp env.example env
```

Run tests:

```bash
docker-compose run back bash test
```

Swagger path(openAPI):

```bash
localhost/docs
```

