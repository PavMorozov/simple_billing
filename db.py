# -*- coding: utf-8 -*-


from sqlalchemy import create_engine
from sqlalchemy.ext.asyncio import AsyncSession, create_async_engine
from sqlalchemy.orm.session import Session
from sqlalchemy.orm import sessionmaker

from settings import DATABASE
from utils.utils import Singleton


class BaseDbClient(metaclass=Singleton):
    _engine = None
    _session_maker = None
    _session = None
    _driver = None

    @classmethod
    def make_engine_url(cls) -> str:
        return f"postgresql+{cls._driver}://{DATABASE['USER']}:{DATABASE['PASSWORD']}" \
                         f"@{DATABASE['HOST']}:{DATABASE['PORT']}/{DATABASE['NAME']}"

    @classmethod
    def get_engine(cls):
        raise NotImplementedError

    @classmethod
    def _get_session_maker(cls) -> sessionmaker:
        if not cls._session_maker:
            cls.get_engine()
            cls._session_maker = sessionmaker(
                cls._engine, class_=cls._session, expire_on_commit=False
            )
        return cls._session_maker

    @classmethod
    def get_session(cls) -> Session:
        session_maker = cls._get_session_maker()
        return session_maker()

class AsyncDbClient(BaseDbClient):
    _driver = 'asyncpg'
    _session = AsyncSession

    @classmethod
    def get_engine(cls):
        if not cls._engine:
            engine_url = cls.make_engine_url()
            cls._engine = create_async_engine(
                engine_url, echo=True, pool_size=50, max_overflow=20
            )
        return cls._engine


class SyncDbClient(BaseDbClient):
    _driver = 'psycopg2'
    _session = Session

    @classmethod
    def get_engine(cls):
        if not cls._engine:
            engine_url = cls.make_engine_url()
            cls._engine = create_engine(
                engine_url, echo=False, pool_size=50, max_overflow=20
            )

        return cls._engine
