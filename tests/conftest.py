# -*- coding: utf-8 -*-


import pytest
from sqlalchemy_utils import database_exists, create_database

from api.models import Base, User, Wallet
from db import SyncDbClient


@pytest.fixture(scope="session")
def engine():
    engine = SyncDbClient.get_engine()
    if not database_exists(engine.url):
        create_database(engine.url)
    return engine


@pytest.fixture()
def tables(engine):
    Base.metadata.create_all(engine)
    yield
    Base.metadata.drop_all(engine)


@pytest.fixture
def db_session(engine, tables):
    connection = engine.connect()
    transaction = connection.begin()
    session = SyncDbClient.get_session()

    yield session

    session.close()
    transaction.rollback()
    connection.close()


@pytest.fixture()
def user_data():
    return {'username': 'user 1',
             'email': 'mail@s.y',
             'password': 'any_pass'}


@pytest.fixture()
def other_user_data():
    return {'username': 'user2',
             'email': 'mail2@s.y',
             'password': 'any_pass2'}


@pytest.fixture()
def create_users(db_session, user_data, other_user_data):
    wallets = [Wallet(user=User(**user_data)),
               Wallet(user=User(**other_user_data))]
    db_session.add_all(wallets)
    db_session.commit()

