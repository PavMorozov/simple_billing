# -*- coding: utf-8 -*-


import copy
import decimal

from fastapi.testclient import TestClient
from sqlalchemy.sql import select

from api.models import User, Wallet, Transaction
from main import app


client = TestClient(app)


def test_create_user(engine, db_session, user_data):
    right_user_data = user_data
    response = client.post('/users', json=right_user_data)
    assert response.json() == right_user_data


def test_unique_fields_with_create_user(db_session, user_data, other_user_data):
    unique_fields = ['username', 'email']
    right_user_data = user_data
    r = client.post('/users', json=right_user_data)
    for field in unique_fields:
        new_data = copy.copy(other_user_data)
        new_data[field] = right_user_data[field]
        response = client.post('/users', json=new_data)
        assert response.json() == {'message': 'duplicate key value violates unique constraint'}

# todo: max_len tests, email format


def test_create_wallet_with_create_user(db_session, user_data):
    result = db_session.query(User).all()
    assert not result
    result = db_session.query(Wallet).all()
    assert not result
    right_user_data = user_data
    client.post('/users', json=right_user_data)
    query = select(User).where(User.username == right_user_data['username'])
    result = db_session.execute(query).scalars().first()

    assert result.wallet

def test_change_user_balance_with_wallet_put(db_session, create_users):
    summ = 11.23
    user_1, user_2 = db_session.query(User).all()

    wallet_data = {'user_id': user_1.id, 'summ': summ}
    client.put('/wallets', json=wallet_data)
    db_session.expire_all()
    wallet = db_session.get(Wallet, user_1.wallet.id)
    assert wallet.balance == decimal.Decimal(str(summ))
    transaction = db_session.query(Transaction).first()
    assert transaction.summ == decimal.Decimal(str(summ))
    assert transaction.sender_id is None
    assert transaction.recipient_id == user_1.id


def test_summ_with_wallet_put(db_session, create_users):
    summ = -11.23
    user_1, user_2 = db_session.query(User).all()

    wallet_data = {'user_id': user_1.id, 'summ': summ}
    response = client.put('/wallets', json=wallet_data)
    assert response.status_code == 400
    assert response.json() == [{'summ': 'ensure this value is greater than 0'}]


#todo: wallet put: check  user is not exists, etc.

def test_transaction_post(db_session, create_users):
    transaction_summ = 21.17
    sender_balance = 300
    sender, recipient = db_session.query(User).all()
    sender.wallet.balance = sender_balance
    db_session.commit()
    transaction_data = {'sender_id': sender.id,
                       'recipient_id': recipient.id,
                       'summ': transaction_summ}
    client.post('/transactions', json=transaction_data)
    db_session.expire_all()
    transaction = db_session.query(Transaction).first()
    assert (transaction.sender_id, transaction.recipient_id, transaction.summ) == (sender.id, recipient.id,
                                                                                   decimal.Decimal(str(transaction_summ)))
    sender_wallet = db_session.get(Wallet, sender.wallet.id)
    assert sender_wallet.balance == sender_balance - decimal.Decimal(str(transaction_summ))

    recipient_wallet = db_session.get(Wallet, recipient.wallet.id)
    assert recipient_wallet.balance == decimal.Decimal(str(transaction_summ))

def test_transacion_summ_is_bigger_than_sender_balance(db_session, create_users):
    transaction_summ = 21.17
    sender, recipient = db_session.query(User).all()
    transaction_data = {'sender_id': sender.id,
                       'recipient_id': recipient.id,
                       'summ': transaction_summ}
    response = client.post('/transactions', json=transaction_data)
    assert response.status_code == 400
    assert response.json() == [{'summ': "summ is bigger than sender's balance"}]