# -*- coding: utf-8 -*-


from sqlalchemy.future import select

from fastapi import APIRouter

from api.models import User, Wallet, Transaction
from api.serializers import UserPydantic, WalletPydantic, TransactionPydantic
from db import AsyncDbClient as DbClient
from utils.exceptions import RouterValidationException


router = APIRouter()


@router.post('/users', response_model=UserPydantic)
async def create_user(user: UserPydantic):
    async with DbClient.get_session() as session:
        async with session.begin():
            session.add(Wallet(user=User(**user.dict())))
    return user


@router.put('/wallets', response_model=WalletPydantic)
async def update_wallet_sum(data: WalletPydantic):  # todo: here user must get from auth but not send id in request
    async with DbClient.get_session() as session:     # todo: handle user does not exists, etc
        async with session.begin():  # todo: something wrong... redo this block
            session.add(Transaction(recipient=await session.get(User, data.user_id), summ=data.summ))
            query = select(Wallet).where(Wallet.user_id == data.user_id).with_for_update()
            executed_query = await session.execute(query)
            wallet = executed_query.scalars().first()
            data.summ += wallet.balance
            wallet.balance = data.summ
    return data

@router.post('/transactions', response_model=TransactionPydantic)
async def create_transaction(data: TransactionPydantic):
    async with DbClient.get_session() as session:
        async with session.begin():  # todo: something wrong with relations... redo this block
            session.add(Transaction(recipient=await session.get(User, data.recipient_id),
                                    sender=await session.get(User, data.sender_id),
                                    summ=data.summ)
                        )
            sender_query = select(Wallet).where(Wallet.user_id == data.sender_id).with_for_update()
            executed_sender_query = await session.execute(sender_query)
            sender_wallet = executed_sender_query.scalars().first()
            if data.summ > sender_wallet.balance:  # todo: move to serializer
                raise RouterValidationException([{'summ': "summ is bigger than sender's balance"}])
            recipient_query = select(Wallet).where(Wallet.user_id == data.recipient_id).with_for_update()
            executed_recipient_query = await session.execute(recipient_query)
            recipient_wallet = executed_recipient_query.scalars().first()
            sender_wallet.balance -= data.summ
            recipient_wallet.balance += data.summ
    return data