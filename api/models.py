# -*- coding: utf-8 -*-


import datetime

from sqlalchemy import Integer, String, Column, \
    DateTime,  ForeignKey, DECIMAL, CheckConstraint
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship


Base = declarative_base()


class User(Base):
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True)
    username = Column(String(100), nullable=False, unique=True)
    email = Column(String(100), nullable=False, unique=True)
    password = Column(String(200), nullable=False)
    wallet = relationship('Wallet',  back_populates='user', uselist=False)


class Wallet(Base):
    __tablename__ = 'wallets'
    id = Column(Integer, primary_key=True)
    balance = Column(DECIMAL(scale=2, precision=8), default=0)  # decided, that balance can be < 0
    user_id = Column(Integer, ForeignKey('users.id'), nullable=False)
    user = relationship("User", cascade="all,delete", back_populates="wallet")
    CheckConstraint('balance > 0', name='check_balance')


class Transaction(Base):
    __tablename__ = 'transactions'
    id = Column(Integer, primary_key=True)
    datetime = Column(DateTime, default=datetime.datetime.now)
    sender_id = Column(Integer, ForeignKey('users.id'))
    sender = relationship("User", foreign_keys=[sender_id])
    recipient_id = Column(Integer, ForeignKey('users.id'), nullable=False)
    recipient = relationship("User", foreign_keys=[recipient_id])
    summ = Column(DECIMAL(scale=2, precision=8), nullable=False)


