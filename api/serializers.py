# -*- coding: utf-8 -*-


from pydantic import BaseModel, constr, EmailStr, condecimal, conint, validator


class UserPydantic(BaseModel):
    username: constr(max_length=70)
    password: constr(max_length=70)
    email: EmailStr

    class Config:
        orm_mode = True


class WalletPydantic(BaseModel):
    user_id: conint(gt=0)
    summ: condecimal(gt=0, max_digits=8, decimal_places=2)


class TransactionPydantic(BaseModel):
    sender_id: conint(gt=0)
    recipient_id: conint(gt=0)
    summ: condecimal(gt=0, max_digits=8, decimal_places=2)