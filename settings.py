# -*- coding: utf-8 -*-


import os


DATABASE = {
    'NAME': os.environ.get("POSTGRES_DB", "postgres"),
    "HOST": os.environ.get("POSTGRES_HOST", "localhost"),
    "PORT": os.environ.get("POSTGRES_PORT", "5432"),
    "USER": os.environ.get("POSTGRES_USER", "postgres"),
    "PASSWORD": os.environ.get("POSTGRES_PASSWORD", ""),
}
