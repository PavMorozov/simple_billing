# -*- coding: utf-8 -*-


from fastapi import FastAPI, Request
from fastapi.exceptions import RequestValidationError
from fastapi.responses import JSONResponse, PlainTextResponse
from pydantic import ValidationError
from sqlalchemy.exc import IntegrityError, SQLAlchemyError

from api import routers
from db import AsyncDbClient as DbClient
from utils.exceptions import RouterValidationException
from utils.utils import convert_default_exception_message


app = FastAPI()
app.include_router(routers.router)


@app.exception_handler(SQLAlchemyError)
async def alchemy_exception_handler(request: Request, exc: SQLAlchemyError):
    content = {'message': f"Can't save data to db"}
    if isinstance(exc, IntegrityError):  # todo: need to handle other types
        content['message'] = 'duplicate key value violates unique constraint'
    return JSONResponse(
        status_code=400,
        content=content,
    )

@app.exception_handler(RequestValidationError)
async def validation_exception_handler(request: Request, exc: ValidationError):
    return PlainTextResponse(convert_default_exception_message(exc), status_code=400)


@app.exception_handler(RouterValidationException)
async def router_validation_exception_handler(request: Request, exc: RouterValidationException):
    return JSONResponse(
        status_code=400,
        content=exc.message,
    )


@app.on_event("startup")
async def startup():
    DbClient.get_engine()


@app.on_event("shutdown")
async def shutdown():
    await DbClient.get_engine().dispose()

