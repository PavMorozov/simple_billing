# -*- coding: utf-8 -*-


from typing import List, Dict


class RouterValidationException(ValueError):
    def __init__(self, message: List[Dict]):
        self.message = message

