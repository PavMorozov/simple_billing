# -*- coding: utf-8 -*-

import json
from pydantic import ValidationError


def convert_default_exception_message(exc: ValidationError) -> str:
    return json.dumps([{i['loc'][-1]: i['msg']} for i in exc.errors()])

class Singleton(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super().__call__(*args, **kwargs)
        return cls._instances[cls]

